'use strict';

const assert = require('assert');
const sinon = require('sinon');
const mockConfig = require('./mock-config');
const logger = require('../lib/logger');
const errorReporter = require('../lib/error-reporter');

describe('error-reporter', () => {
  let config;
  let loggerInstance;
  let errorReporterInstance;

  beforeEach(() => {
    config = mockConfig({
      errorReporting: {
        enabled: true,
      },
    });

    // TODO We could do with mocks of these
    loggerInstance = logger.create({ config });
    errorReporterInstance = errorReporter.create({
      config,
      logger: loggerInstance,
    });
  });

  it('should log errors', () => {
    sinon.spy(loggerInstance, 'error');

    errorReporterInstance(new Error('some test error'), {}, {});

    assert.equal(
      loggerInstance.error.callCount,
      0,
      'should not encounter any errors when trying to report an error to Sentry'
    );
  });
});
