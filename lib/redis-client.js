'use strict';

const redis = require('redis');
const shutdown = require('shutdown');

const clients = [];
const _ = require('lodash');
const debug = require('debug')('gitter:redis-client');
const Promise = require('bluebird');
const parseRedisOptions = require('./utils/redis-url-parser');

/**
 * Switches redis database
 */
function switchDatabase(client, redisDb, logger) {
  if (redisDb) {
    debug('Switching redis database to #%s', redisDb);
    client.select(redisDb, (err) => {
      if (err) {
        logger.error('Unable to switch redis databases', err);
        /* This could be bad.. Need to consider a better way of handling it */
        throw err;
      }
    });
  }
}

/**
 * Creates a sentinel client
 */
function createSentinelClientInstance(sentinelOpts, clientOpts, logger) {
  const RedisSentinel = require('@gitterhq/redis-sentinel-client');

  /* redis-sentinel will always try redis sentinels from first to last
     so shuffle the list to ensure that no sentinel is slacking */
  const sentinels = _.shuffle(sentinelOpts.hosts).map((f) => {
    const a = f.split(':');
    return [a[0], parseInt(a[1], 10)];
  });

  const sentinelClient = RedisSentinel.createClient({
    sentinels,
    masterName: sentinelOpts['master-name'],
    masterOptions: clientOpts, // Beware! Bit confusing this naming...
  });
  sentinelClient.setMaxListeners(50);

  if (debug.enabled) {
    ['sentinel connected', 'sentinel disconnected'].forEach((event) => {
      sentinelClient.on(event, () => {
        debug('Redis sentinel client: %s', event);
      });
    });
  }

  ['failover start', 'failover end', 'switch master'].forEach((event) => {
    sentinelClient.on(event, () => {
      logger.warn(`Redis sentinel client: ${event}`);
    });
  });

  sentinelClient.on('sentinel message', (msg) => {
    logger.info(`Redis sentinel client: sentinel message: ${msg}`);
  });

  sentinelClient.on('error', (err) => {
    logger.error(`Redis error (node_redis sentinel): ${err}`, {
      exception: err,
    });
  });

  function validateConnection(client) {
    if (
      !client.host.match(
        new RegExp(`^${sentinelOpts.validateHost.replace(/\./g, '\\.').replace(/\*/g, '\\d+')}$`)
      )
    ) {
      logger.error('**************************************');
      logger.error('* HERE BE DRAGONS                    *');
      logger.error('**************************************');
      logger.error(`Host ${client.host} does not match ${sentinelOpts.validateHost}.`);
      logger.error("This is almost certainly not what you're looking to do");
      logger.error('**************************************');
    }
  }

  if (sentinelOpts.validateHost) {
    if (sentinelClient.activeMasterClient) {
      validateConnection(sentinelClient.activeMasterClient);
    } else {
      sentinelClient.once('reconnected', () => {
        validateConnection(sentinelClient.activeMasterClient);
      });
    }
  }

  return sentinelClient;
}

function createInstance(options, logger) {
  debug('Creating redis instance  with options %j', options);

  let client;
  if (typeof options === 'string') {
    options = parseRedisOptions(options);
  }

  if (options.sentinel) {
    client = createSentinelClientInstance(options.sentinel, options.clientOpts, logger);
  } else {
    const { host } = options;
    const { port } = options;

    client = redis.createClient(port, host, options.clientOpts);
    client.on('error', (err) => {
      logger.error(`Redis error (node_redis): ${err}`, { exception: err });
    });
  }
  switchDatabase(client, options.redisDb, logger);
  return client;
}

function registerClient(client) {
  clients.push(client);
}

function setupTransientClient(port, host, redisDb, clientOpts, logger) {
  const client = redis.createClient(port, host, clientOpts);
  client.on('error', (err) => {
    logger.error(`Redis error (transient node_redis): ${err}`, {
      exception: err,
    });
  });
  switchDatabase(client, redisDb, logger);

  /*
   * Add some logging if the transient client is not closed
   */
  const timeout = setTimeout(() => {
    logger.warn('Transient redis connection has not been closed after 3 minutes.');
  }, 180000);

  client.once('end', () => {
    clearTimeout(timeout);
  });

  registerClient(client);
  return client;
}

exports.createTransientClient = function (mainClient, options, logger, callback) {
  let host;
  let port;

  if (typeof options === 'string') {
    options = parseRedisOptions(options);
  }

  const { redisDb } = options;
  const { clientOpts } = options;

  if (options.sentinel) {
    const master = mainClient.activeMasterClient;

    /* No master yet? Wait... */
    if (!master) {
      mainClient.on('reconnected', () => {
        const client = setupTransientClient(
          mainClient.activeMasterClient.port,
          mainClient.activeMasterClient.host,
          redisDb,
          clientOpts,
          logger
        );
        callback(null, client);
      });

      return;
    }

    host = master.host;
    port = master.port;
  } else {
    host = options.host;
    port = options.port;
  }

  const client = setupTransientClient(port, host, redisDb, clientOpts, logger);

  /* Callback in a second */
  setImmediate(() => {
    callback(null, client);
  });
};

shutdown.addHandler('redis', 1, (callback) =>
  Promise.map(clients, (client) =>
    Promise.fromCallback((callback) => {
      // Workaround for bug in redis-sentinel client
      // until this patched
      // Callback called multiple times on quit
      let callbackCount = client.activeMasterClient ? 3 : 1;

      quit(client, () => {
        callbackCount--;
        if (callbackCount === 0) callback();
      });
    })
  ).asCallback(callback)
);

exports.create = function (options, logger) {
  const client = createInstance(options, logger);
  registerClient(client);

  return client;
};

function quit(client, callback) {
  for (let i = 0; i < clients.length; i++) {
    if (clients[i] === client) {
      clients.splice(i, 1);
      break;
    }
  }

  client.quit(callback);
}
exports.quit = quit;

exports.testOnly = {
  getClients() {
    return clients;
  },
};
