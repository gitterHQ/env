'use strict';

describe('gitter-stats', () => {
  it('should fire off all the events correctly', () => {
    const config = require('../lib/config').create(`${__dirname}/config/stats`);
    const logger = require('../lib/logger').create({ config });
    const stats = require('../lib/stats').create({ logger, config });

    stats.event('testEvent');
    stats.eventHF('testEventHF');
    stats.gaugeHF('testGaugeHF');
    // stats.userUpdate('test');
    stats.responseTime('testResponseTime', 1);
  });

  // FIXME: Once redis gets fixed, this tests will start working
  xit('should fire off google events', (done) => {
    const config = {
      get(name) {
        if (name === 'stats:ga:key') {
          return 'UA-45918290-3';
        }
      },
      getBool(name) {
        if (name === 'stats:ga:enabled') {
          return true;
        }
      },
      events: {
        on() {},
      },
    };
    const logger = require('../lib/logger').create({ config });
    const stats = require('../lib/stats').create({ logger, config });

    stats.event('test', { googleAnalyticsUniqueId: '1644931976.1385378625' });
    setTimeout(done, 100);
  });
});
