'use strict';

const Promise = require('bluebird');
const mandrillClient = require('mandrill-api/mandrill');

function createMandrillMailer(options) {
  const { config } = options;
  const { stats } = options;
  const { logger } = options;

  const mandrillApiKey = config.get('mandrill:apiKey');
  const mandrill = new mandrillClient.Mandrill(mandrillApiKey);

  function sendTemplate(options) {
    return new Promise((resolve, reject) => {
      mandrill.messages.sendTemplate(
        options,
        (result) => {
          const first = result[0];
          if (first.status === 'rejected' || first.status === 'invalid') {
            return reject(
              new Error(`Email provider rejected email: ${first.reject_reason || first.status}`)
            );
          }

          /* Resolve */
          resolve({
            id: first._id,
            status: first.status,
          });
        },
        (err) => {
          if (!(err instanceof Error) && err.message) {
            return reject(new Error(err.message));
          }

          reject(err);
        }
      );
    });
  }

  /* Resolve all the values in a hash */
  function resolvePromises(data) {
    if (!data || typeof data !== 'object') {
      return Promise.resolve(data);
    }

    const keys = Object.keys(data);
    const values = keys.map((k) => data[k]);
    return Promise.all(values).then((values) =>
      /* Create a copy of the data, with the resolved values */
      values.reduce((memo, value, index) => {
        const key = keys[index];
        memo[key] = value;
        return memo;
      }, {})
    );
  }

  function convertHashToMandrillVarsArray(hash) {
    return Object.keys(hash).map((key) => ({ name: key, content: hash[key] }));
  }

  /**
   * Send an email and return a promise of
   * { id: XXXX, status: 'queued/sent' }
   *
   * Required parameters:
   *   templateName:
   *   subject:
   *   to: (email address)
   *   data: a hash containing values or promises to be resolved
   */
  return function sendMail(options) {
    const { templateName } = options;
    if (!templateName) throw new Error('templateName required');

    /* Ensure that all promises are resolved */
    return resolvePromises(options.data)
      .then((data) => {
        const to = [{ email: options.to, type: 'to' }];

        if (options.bcc) {
          to.push({ email: options.bcc, type: 'bcc' });
        }

        const headers = options.replyTo ? { 'Reply-To': options.replyTo } : undefined;

        return sendTemplate({
          template_name: templateName,
          template_content: convertHashToMandrillVarsArray(data),
          // API definition: https://mandrillapp.com/api/docs/messages.nodejs.html
          message: {
            subject: options.subject,
            from_email: options.fromEmail,
            from_name: options.fromName,
            to,
            headers,
            tags: [options.templateName], // used for A/B testing
            global_merge_vars: convertHashToMandrillVarsArray(data),
          },
        });
      })
      .then((result) => {
        /* Email sent successfully */
        logger.verbose(`Sent email: ${templateName}: ${result.id}`);
        stats.event('email_sent');

        const { tracking } = options;
        if (tracking) {
          stats.event(tracking.event, tracking.data);
        }

        return result;
      })
      .catch((err) => {
        /* Email sent failure */
        logger.error(`Email send failed: ${err}`, { exception: err });

        stats.event('email_send_failed');
        throw err;
      });
  };
}

exports.create = createMandrillMailer;
