'use strict';

const assert = require('assert');
const RedisController = require('./redis-controller');
const debug = require('debug')('gitter:env:test:redis-spec');

module.exports = function redisSpec(create, quit) {
  let redisController;

  before((done) => {
    redisController = new RedisController();
    done();
  });

  beforeEach(() => {
    redisController.ensureAllStarted();
  });

  after((done) => {
    redisController.cleanup();
    done();
  });

  describe('failover', function () {
    this.timeout(15000);
    let mainRedisClient;

    beforeEach(() => {
      mainRedisClient = create(redisController.options);
    });

    afterEach((done) => {
      quit(mainRedisClient);
      setTimeout(done, 500);
    });

    it('should obtain a handle failover correctly', (done) => {
      const setValue = String(Math.random());

      mainRedisClient.set('moo', setValue, (err) => {
        if (err) return done(err);

        redisController.stop('redis1');

        mainRedisClient.get('moo', (err, value) => {
          redisController.ensureAllStarted();

          if (err) return done(err);

          assert.strictEqual(value, setValue);

          done();
        });
      });
    });

    it('should handle mega failure correctly', (done) => {
      const setValue = String(Math.random());

      debug('setting value');
      mainRedisClient.set('s', setValue, (err) => {
        if (err) return done(err);

        // Kill the sentinel and the redis
        debug('terminating sentinel1 and redis1');
        redisController.stop('sentinel1');
        redisController.stop('redis1');

        setTimeout(() => {
          redisController.ensureAllStarted();

          function next() {
            debug('trying to read value again');

            mainRedisClient.get('s', (err) => {
              if (err) {
                debug('failed with: %s', err.stack || err);
                setTimeout(next, 500);
                return;
              }

              done();
            });
          }

          next();
        }, 1500);
      });
    });

    it('should handle sentinel failure correctly', (done) => {
      const setValue = String(Math.random());

      mainRedisClient.set('s', setValue, (err) => {
        if (err) return done(err);

        redisController.stop('sentinel1');

        setTimeout(() => {
          mainRedisClient.get('s', (err, value) => {
            if (err) return done(err);

            assert.strictEqual(value, setValue);
            done();
          });
        }, 500);
      });
    });

    it('should handle sentinel failure prior to initial connection', (done) => {
      redisController.stop('sentinel1');
      redisController.stop('sentinel2');
      redisController.stop('sentinel3');

      setTimeout(() => {
        redisController.ensureAllStarted();

        const setValue = String(Math.random());

        mainRedisClient.set('s', setValue, (err) => {
          if (err) return done(err);

          mainRedisClient.get('s', (err, value) => {
            if (err) return done(err);

            assert.strictEqual(value, setValue);
            done();
          });
        });
      }, 1000);
    });
  });
};
