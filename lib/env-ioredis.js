'use strict';

exports.create = function (options) {
  const Redis = require('ioredis');
  const redisUrlParser = require('./utils/redis-url-parser');
  const _ = require('lodash');
  const shutdown = require('shutdown');

  const { logger } = options;
  const { config } = options;

  function connectionToIoRedis(connection, extras) {
    const result = {};
    if (connection.sentinel) {
      /* ioredis will always try redis sentinels from first to last
         so shuffle the list to ensure that no sentinel is slacking */
      const sentinels = _.shuffle(connection.sentinel.hosts).map((hostString) => {
        const s = hostString.split(':');
        const port = parseInt(s[1], 10) || 26379;
        return { host: s[0], port };
      });

      result.sentinels = sentinels;
      result.name = connection.sentinel['master-name'];
    }

    if (connection.redisDb) {
      result.db = connection.redisDb;
    }

    if (connection.host) {
      result.host = connection.host;
    }

    if (connection.port) {
      result.port = connection.port;
    }

    if (extras) {
      _.extend(result, extras);
    }

    if (process.env.NODE_ENV === 'dev') {
      // Super useful stacktraces through Redis
      result.showFriendlyErrorStack = true;
    }

    return result;
  }

  const redisClients = [];

  shutdown.addHandler('redis', 1, (callback) => {
    function next() {
      if (!redisClients.length) return;
      const client = redisClients.pop();
      return client
        .quit()
        .catch((err) => {
          logger.error(`Error while quitting redis client: ${err}`, {
            exception: err,
          });
        })
        .then(next);
    }

    next().nodeify(callback);
  });

  return {
    // Create a non-singleton client
    createClient(connection, extras) {
      connection = connection || process.env.REDIS_CONNECTION_STRING || config.get('redis');
      if (typeof connection === 'string') {
        connection = redisUrlParser(connection);
      }
      const redisConnection = connectionToIoRedis(connection, extras);

      const redis = new Redis(redisConnection);
      redis.on('error', (err) => {
        logger.error(`Redis error (ioredis): ${err}`, { exception: err });
      });

      redisClients.push(redis);
      return redis;
    },

    quitClient(client) {
      return client.quit().then(() => {
        const index = redisClients.indexOf(client);
        if (index >= 0) {
          redisClients.splice(index, 1);
        }
      });
    },

    parse(connectionString) {
      return redisUrlParser(connectionString);
    },
  };
};
