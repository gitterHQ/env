'use strict';

const _ = require('lodash');
const NodeMailerMailer = require('./nodemailer-mailer');

function create(createOptions) {
  const { config } = createOptions;
  // eslint-disable-next-line global-require
  const nodemailer = require('nodemailer');
  // eslint-disable-next-line global-require
  const ses = require('nodemailer-ses-transport');

  const transporter = nodemailer.createTransport(
    ses({
      accessKeyId: config.get('amazonses:accessKeyId'),
      secretAccessKey: config.get('amazonses:secretAccessKey'),
    })
  );

  const sesMailer = new NodeMailerMailer(
    transporter,
    _.defaults({}, createOptions, {
      defaultFromEmailName: config.get('amazonses:defaultFromEmailName'),
      defaultFromEmailAddress: config.get('amazonses:defaultFromEmailAddress'),
      overrideTo: config.get('amazonses:overrideTo'),
    })
  );

  return (options) => sesMailer.send(options);
}

module.exports = {
  create,
};
