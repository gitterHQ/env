'use strict';

const path = require('path');

/**
 * These functions are used to determine the current app name.
 * Before each function there is an example of the returned
 * value when the input is "gitter-webapp-1".
 *
 * Returns "gitter-webapp-1"
 */
function getFullProcessName() {
  const upstartJob = process.env.UPSTART_JOB || process.env.JOB;
  if (upstartJob) return upstartJob;

  const { main } = require;
  if (!main || !main.filename) return 'unknown';

  return path.basename(main.filename, '.js');
}

/* Returns "gitter-webapp" */
function getGenericProcessName() {
  return getFullProcessName().replace(/-\d*$/, '');
}

/* Returns "webapp" */
function getShortProcessName() {
  return getGenericProcessName().replace(/^gitter-/, '');
}

module.exports = {
  getFullProcessName,
  getGenericProcessName,
  getShortProcessName,
};
