'use strict';

const assert = require('assert');
const redisUrlParser = require('../lib/utils/redis-url-parser');

describe('redis-url-parser', () => {
  it('should parse a simple url', () => {
    assert.deepEqual(redisUrlParser('redis://localhost'), {
      host: 'localhost',
    });
  });

  it('should parse a simple url with port', () => {
    assert.deepEqual(redisUrlParser('redis://localhost:6379'), {
      host: 'localhost',
      port: 6379,
    });
  });

  it('should parse a simple url with port and db', () => {
    assert.deepEqual(redisUrlParser('redis://localhost:6379?db=1'), {
      host: 'localhost',
      port: 6379,
      redisDb: 1,
    });
  });

  it('should parse a simple url with port, db and clientOpts ', () => {
    assert.deepEqual(redisUrlParser('redis://localhost:6379?db=1&return_buffers=true'), {
      host: 'localhost',
      port: 6379,
      redisDb: 1,
      clientOpts: { return_buffers: true },
    });
  });

  it('should parse a sentinel connection', () => {
    assert.deepEqual(redisUrlParser('redis-sentinel://localhost:26379?master=mymaster&db=1'), {
      redisDb: 1,
      sentinel: {
        'master-name': 'mymaster',
        hosts: ['localhost:26379'],
      },
    });
  });

  it('should parse a sentinel connection with multiple hosts', () => {
    assert.deepEqual(
      redisUrlParser('redis-sentinel://host01:26379,host02:26379?master=mymaster&db=1'),
      {
        redisDb: 1,
        sentinel: {
          'master-name': 'mymaster',
          hosts: ['host01:26379', 'host02:26379'],
        },
      }
    );
  });

  it('should parse a sentinel connection with multiple hosts and client options', () => {
    assert.deepEqual(
      redisUrlParser(
        'redis-sentinel://host01:26379,host02:26379?master=mymaster&db=1&return_buffers=true'
      ),
      {
        redisDb: 1,
        sentinel: {
          'master-name': 'mymaster',
          hosts: ['host01:26379', 'host02:26379'],
        },
        clientOpts: {
          return_buffers: true,
        },
      }
    );
  });
});
