/* jshint node:true, unused: true */

'use strict';

const blocked = require('blocked');

exports.create = function (options) {
  const { config } = options;

  if (config.get('blockMonitor:enabled') === false) return;

  function installBlocked() {
    const statsdClient = require('./stats-client').create({
      config,
      includeNodeVersionTags: true,
    });

    blocked((ms) => {
      statsdClient.histogram('eventloop.block', ms);
    });
  }

  // Give the process 5seconds to start before reporting blockages
  const startupDelay = config.get('blockMonitor:startupDelay') || 5000;
  setTimeout(installBlocked, startupDelay);
};
