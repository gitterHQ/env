/* jshint globalstrict:true, trailing:false, unused:true, node:true */

'use strict';

exports.create = function (configDirectory) {
  const config = require('./config').create(configDirectory);

  const logger = require('./logger').create({ config });
  const stats = require('./stats').create({ config, logger });
  const errorReporter = require('./error-reporter').create({ config, logger });
  const redisClient = require('./redis-client');
  const errorHandlerMiddleware = require('./middlewares/error-handler');
  const errorReporterMiddleware = require('./middlewares/error-reporter');
  const accessLogger = require('./middlewares/access-logger');
  const identifyRouteMiddleware = require('./middlewares/identify-route');
  const unhandledExceptionHandler = require('./unhandled-exceptions');
  const redisUrlParser = require('./utils/redis-url-parser');

  /* Monitor event loop blockages */
  require('./block-monitor').create({ config, stats, logger });

  /* Main singleton client */
  let mainRedisClient;

  const middlewares = {
    identifyRoute: identifyRouteMiddleware,
  };

  function createErrorReporterMiddleware() {
    const statsPrefix = config.get('stats:statsd:prefix');

    const statsClient = require('./stats-client').create({
      config,
      prefix: statsPrefix,
      includeNodeVersionTags: true,
    });
    return errorReporterMiddleware.create({
      config,
      logger,
      statsClient,
      errorReporter,
    });
  }

  Object.defineProperty(middlewares, 'errorHandler', {
    enumerable: true,
    get() {
      const errorReporterMiddleware = createErrorReporterMiddleware();
      return function (err, req, res, next) {
        /* Invoke the error reporter before calling the handler */
        errorReporterMiddleware(err, req, res, (err) =>
          errorHandlerMiddleware(err, req, res, next)
        );
      };
    },
  });

  Object.defineProperty(middlewares, 'errorReporter', {
    enumerable: true,
    get: createErrorReporterMiddleware,
  });

  Object.defineProperty(middlewares, 'accessLogger', {
    enumerable: true,
    get() {
      return accessLogger.create({
        config,
      });
    },
  });

  var env = {
    config,
    logger,
    errorReporter,
    stats,
    createStatsClient: function createStatsClient(options) {
      return require('./stats-client').create({
        config,
        prefix: options && options.prefix,
        tags: options && options.tags,
        includeNodeVersionTags: options && options.includeNodeVersionTags,
      });
    },

    installUncaughtExceptionHandler() {
      process.on(
        'uncaughtException',
        unhandledExceptionHandler.create({ config, logger, errorReporter })
      );
    },

    domainWrap(inside) {
      /* Only require this where we use it as requiring domains changes the way node works */
      const domain = require('domain');

      // create a top-level domain for the server
      const serverDomain = domain.create();
      serverDomain.on('error', unhandledExceptionHandler.create({ config, logger, errorReporter }));
      serverDomain.run(inside);
    },

    redis: {
      getClient() {
        if (mainRedisClient) return mainRedisClient;

        // no options override for singleton
        const options = process.env.REDIS_CONNECTION_STRING || config.get('redis');
        mainRedisClient = redisClient.create(options, logger);

        return mainRedisClient;
      },

      createClient(options) {
        options = options || process.env.REDIS_CONNECTION_STRING || config.get('redis');
        return redisClient.create(options, logger);
      },

      /**
       * Returns a short-lived connection, which will not be immune to a redis failover
       */
      createTransientClient(callback) {
        const mainClient = this.getClient();

        // no options override for singleton
        const options = process.env.REDIS_CONNECTION_STRING || config.get('redis');
        return redisClient.createTransientClient(mainClient, options, logger, callback);
      },

      quitClient(client) {
        return redisClient.quit(client);
      },

      parse(connectionString) {
        return redisUrlParser(connectionString);
      },
    },

    ioredis: require('./env-ioredis').create({ config, logger }),

    mongo: {
      /* Bring your own mongoose connection configurer */
      configureMongoose(mongoose) {
        return require('./mongoose-connection-configurer')({
          config,
          logger,
          mongoose,
          env,
        });
      },
    },

    middlewares,
  };

  /* Lazy load mailer */
  let mailer;
  Object.defineProperty(env, 'mailer', {
    enumerable: true,
    get() {
      if (mailer) return mailer;
      mailer = require('./mailer').create({ logger, stats, config });
      return mailer;
    },
  });

  return env;
};
