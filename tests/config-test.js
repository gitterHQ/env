'use strict';

const assert = require('assert');

describe('gitter-config', () => {
  it('should load the prod config correctly', () => {
    process.env.NODE_ENV = 'prod';
    const config = require('../lib/config').create(`${__dirname}/config`);

    assert.equal('prodcow', config.get('test:value'));
  });

  it('should load the dev config correctly', () => {
    process.env.NODE_ENV = 'dev';
    const config = require('../lib/config').create(`${__dirname}/config`);

    assert.equal('devcow', config.get('test:value'));
  });

  it('should load the default config correctly', () => {
    process.env.NODE_ENV = 'x';
    const config = require('../lib/config').create(`${__dirname}/config`);

    assert.equal('something', config.get('test:another_value'));
  });
});
