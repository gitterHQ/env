'use strict';

const assert = require('assert');
const redisClient = require('../lib/redis-client');
const mockConfig = require('./mock-config');
const RedisController = require('./redis-controller');
const redisSpec = require('./redis-spec');

describe('gitter-redis', () => {
  let logger;

  before(() => {
    logger = require('../lib/logger').create({ config: mockConfig({}) });
  });

  describe('node_redis tests', () => {
    let redisController;

    before((done) => {
      redisController = new RedisController();
      done();
    });

    beforeEach(() => {
      redisController.ensureAllStarted();
    });

    after(() => {
      redisController.cleanup();
    });

    redisSpec(
      (options) => redisClient.create(options, logger),
      (client) => {
        client.on('error', () => {});
        redisClient.quit(client);
      }
    );

    it('should obtain a connection and release it', (done) => {
      const mainRedisClient = redisClient.create(redisController.options, logger);
      assert.strictEqual(redisClient.testOnly.getClients().length, 1);
      assert(mainRedisClient);

      mainRedisClient.on('reconnected', () => {
        redisClient.quit(mainRedisClient);

        assert.strictEqual(redisClient.testOnly.getClients().length, 0);

        setImmediate(() => {
          // Don't cause problems for other tests after we shut down redis
          mainRedisClient.on('error', () => {});
          done();
        });
      });
    });

    it('should obtain a transient connection and release it', (done) => {
      const mainRedisClient = redisClient.create(redisController.options, logger);

      redisClient.createTransientClient(
        mainRedisClient,
        redisController.options,
        logger,
        (err, temporaryClient) => {
          assert.strictEqual(redisClient.testOnly.getClients().length, 2);

          assert(temporaryClient);

          redisClient.quit(temporaryClient);
          redisClient.quit(mainRedisClient);

          assert.strictEqual(redisClient.testOnly.getClients().length, 0);

          setImmediate(() => {
            // Don't cause problems for other tests after we shut down redis
            temporaryClient.on('error', () => {});
            mainRedisClient.on('error', () => {});
            done();
          });
        }
      );
    });
  });
});
