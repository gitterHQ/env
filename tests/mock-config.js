'use strict';

const events = require('events');

function MockConfig(hash) {
  this.hash = hash;
  this.events = new events.EventEmitter();
}

MockConfig.prototype.get = function (key) {
  const path = key.split(':');
  let root = this.hash;
  for (let i = 0; i < path.length; i++) {
    root = root[path[i]];
    if (root === null || root === undefined) return root;
  }

  return root;
};

MockConfig.prototype.getBool = function (key) {
  const val = this.get(key);
  if (!val) return false;
  return Boolean(JSON.parse(val));
};

module.exports = function (hash) {
  return new MockConfig(hash);
};
