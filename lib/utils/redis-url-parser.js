'use strict';

const url = require('url');

function getClientOpts(query) {
  return Object.keys(query).reduce((memo, key) => {
    const value = query[key];
    if (value === 'true') {
      memo[key] = true;
    } else if (value === 'false') {
      memo[key] = false;
    } else {
      const intVal = parseInt(value, 10);
      if (isNaN(intVal)) {
        memo[key] = value;
      } else {
        memo[key] = intVal;
      }
    }

    return memo;
  }, {});
}
function getRedisInfo(details) {
  const result = {};

  if (details.query.db) {
    result.redisDb = parseInt(details.query.db, 10);
    delete details.query.db;
  }

  if (details.hostname) {
    result.host = details.hostname;
  }

  if (details.port) {
    result.port = parseInt(details.port, 10);
  }

  if (Object.keys(details.query).length) {
    result.clientOpts = getClientOpts(details.query);
  }

  return result;
}

function parseHosts(connectionString) {
  const m = /^redis-sentinel:\/\/([^?/]+)/.exec(connectionString);
  if (!m) return [];
  return m[1].split(',').filter((f) => f);
}

function getSentinelInfo(connectionString, details) {
  const result = {
    sentinel: {
      'master-name': details.query.master,
      hosts: parseHosts(connectionString),
    },
  };

  delete details.query.master;

  if (details.query.db) {
    result.redisDb = parseInt(details.query.db, 10);
    delete details.query.db;
  }

  if (Object.keys(details.query).length) {
    result.clientOpts = getClientOpts(details.query);
  }

  return result;
}

module.exports = function parse(connectionString) {
  const parsed = url.parse(connectionString, true);
  if (parsed.protocol === 'redis:') {
    return getRedisInfo(parsed);
  }

  if (parsed.protocol === 'redis-sentinel:') {
    return getSentinelInfo(connectionString, parsed);
  }

  throw new Error('Invalid redis connection string', connectionString);
};
