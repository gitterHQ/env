'use strict';

const env = require('../../..').create(__dirname);

const { mailer } = env;

return mailer({
  templateName: 'added-to-room',
  subject: 'Hi There',
  to: 'andrew@gitter.im',
  data: {
    FIRST_NAME: 'Mike',
  },
})
  .then((result) => {
    console.log(result);
  })
  .catch((err) => {
    console.log(err.stack);
  });
