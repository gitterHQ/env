'use strict';

function createWinstonContainer(config) {
  const winston = require('winston');
  const processName = require('./process-name');

  function getTransports() {
    const results = [];

    if (config.getBool('logging:logToUDP')) {
      const appName = processName.getGenericProcessName();

      if (appName) {
        const winstonUdp = require('winston-udp').UDP;
        const os = require('os');

        const udpTransport = new winstonUdp({
          host: config.get('logging:udpHost') || '127.0.0.1',
          port: config.get('logging:udpPort') || 24224,
          level: config.get('logging:level'),
          timestamp() {
            return Date.now();
          },
          formatter(options) {
            return JSON.stringify({
              app: appName,
              env: process.env.NODE_ENV,
              level: options.level,
              message: options.message,
              host: os.hostname(),
              meta: options.meta,
            });
          },
        });

        results.push(udpTransport);
      }
    }

    if (!config.getBool('logging:disableConsole') && !process.env.DISABLE_CONSOLE_LOGGING) {
      const consoleTransport = new winston.transports.Console({
        colorize: config.getBool('logging:colorize'),
        timestamp: config.getBool('logging:timestamp'),
        level: config.get('logging:level'),
        prettyPrint: config.get('logging:prettyPrint'),
      });

      results.push(consoleTransport);
    }

    return results;
  }

  const container = new winston.Container({
    transports: getTransports(),
  });

  return container;
}

exports.create = function (options) {
  const transformMeta = require('./transform-meta');

  const { config } = options;
  if (!config) throw new Error('options must contain config');

  const container = createWinstonContainer(config);

  const statsdClient = require('./stats-client').create({
    config,
    prefix: config.get('stats:statsd:prefix'),
  });

  const loggers = {};
  function get(category) {
    if (loggers[category]) {
      return loggers[category];
    }

    // Create and setup a logger
    const logger = container.get(category);
    loggers[category] = logger;

    let logTags;
    if (category) {
      logTags = [`category:${category}`];
    } else {
      logTags = undefined;
    }

    logger.on('logging', (transport, level) => {
      switch (level) {
        case 'warn':
          // Record 20% of warnings
          statsdClient.increment('logged.warn', 1, 0.2, logTags);
          break;
        case 'error':
          // Record 50% of errors
          statsdClient.increment('logged.error', 1, 0.5, logTags);
          break;
        case 'fatal':
          // Record 100% of fatal
          statsdClient.increment('logged.fatal', 1, 1, logTags);
          break;
      }
    });

    logger.on('error', (err) => {
      console.error(`Logging error: ${err}`, err);
      if (err && err.stack) {
        console.error(err.stack);
      }
    });

    logger.rewriters.push((level, msg, meta) => {
      meta = transformMeta(meta);
      if (category) {
        meta.category = category;
      }
      return meta;
    });

    return logger;
  }

  const defaultLogger = get('');
  defaultLogger.get = get;
  return defaultLogger;
};
